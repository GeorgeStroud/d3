﻿namespace INTRO_USERS
{
    partial class Times
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Times));
            this.Appointments = new System.Windows.Forms.Label();
            this.ComboBoxAppInst = new System.Windows.Forms.ComboBox();
            this.Timebutton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.comboboxname = new System.Windows.Forms.ComboBox();
            this.Lgout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Appointments
            // 
            this.Appointments.AutoSize = true;
            this.Appointments.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appointments.Location = new System.Drawing.Point(432, -2);
            this.Appointments.Name = "Appointments";
            this.Appointments.Size = new System.Drawing.Size(230, 39);
            this.Appointments.TabIndex = 1;
            this.Appointments.Text = "Appointments";
            // 
            // ComboBoxAppInst
            // 
            this.ComboBoxAppInst.FormattingEnabled = true;
            this.ComboBoxAppInst.Location = new System.Drawing.Point(189, 106);
            this.ComboBoxAppInst.Name = "ComboBoxAppInst";
            this.ComboBoxAppInst.Size = new System.Drawing.Size(146, 21);
            this.ComboBoxAppInst.TabIndex = 2;
            // 
            // Timebutton
            // 
            this.Timebutton.Location = new System.Drawing.Point(189, 213);
            this.Timebutton.Name = "Timebutton";
            this.Timebutton.Size = new System.Drawing.Size(146, 23);
            this.Timebutton.TabIndex = 4;
            this.Timebutton.Text = "button1";
            this.Timebutton.UseVisualStyleBackColor = true;
            this.Timebutton.Click += new System.EventHandler(this.Timebutton_Click_1);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(392, 84);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(321, 498);
            this.listBox1.TabIndex = 5;
            // 
            // comboboxname
            // 
            this.comboboxname.FormattingEnabled = true;
            this.comboboxname.Location = new System.Drawing.Point(189, 156);
            this.comboboxname.Name = "comboboxname";
            this.comboboxname.Size = new System.Drawing.Size(146, 21);
            this.comboboxname.TabIndex = 6;
            // 
            // Lgout
            // 
            this.Lgout.Location = new System.Drawing.Point(68, 581);
            this.Lgout.Name = "Lgout";
            this.Lgout.Size = new System.Drawing.Size(297, 89);
            this.Lgout.TabIndex = 7;
            this.Lgout.Text = "LogOut";
            this.Lgout.UseVisualStyleBackColor = true;
            this.Lgout.Click += new System.EventHandler(this.Lgout_Click);
            // 
            // Times
            // 
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1387, 682);
            this.Controls.Add(this.Lgout);
            this.Controls.Add(this.comboboxname);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Timebutton);
            this.Controls.Add(this.ComboBoxAppInst);
            this.Controls.Add(this.Appointments);
            this.Name = "Times";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label Timeslabel1;
        private System.Windows.Forms.Label Appointments;
        private System.Windows.Forms.ComboBox ComboBoxAppInst;
        private System.Windows.Forms.Button Timebutton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox comboboxname;
        private System.Windows.Forms.Button Lgout;
    }
}