﻿namespace INTRO_USERS
{
    partial class Adminstuff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adminstuff));
            this.Adlabel1 = new System.Windows.Forms.Label();
            this.Adlabel2 = new System.Windows.Forms.Label();
            this.Adlabel3 = new System.Windows.Forms.Label();
            this.textBoxInuse = new System.Windows.Forms.TextBox();
            this.textBoxInpass = new System.Windows.Forms.TextBox();
            this.textBoxInfn = new System.Windows.Forms.TextBox();
            this.textBoxInln = new System.Windows.Forms.TextBox();
            this.textBoxInph = new System.Windows.Forms.TextBox();
            this.textBoxInem = new System.Windows.Forms.TextBox();
            this.AdBtn1 = new System.Windows.Forms.Button();
            this.Adlabel4 = new System.Windows.Forms.Label();
            this.Adlabel5 = new System.Windows.Forms.Label();
            this.Adlabel6 = new System.Windows.Forms.Label();
            this.Adlabel7 = new System.Windows.Forms.Label();
            this.Adlabel8 = new System.Windows.Forms.Label();
            this.Adlabel9 = new System.Windows.Forms.Label();
            this.Adlabel10 = new System.Windows.Forms.Label();
            this.Adlabel11 = new System.Windows.Forms.Label();
            this.Adlabel12 = new System.Windows.Forms.Label();
            this.Adlabel13 = new System.Windows.Forms.Label();
            this.Adlabel14 = new System.Windows.Forms.Label();
            this.textBoxAduse = new System.Windows.Forms.TextBox();
            this.textBoxAdpass = new System.Windows.Forms.TextBox();
            this.textBoxAdfn = new System.Windows.Forms.TextBox();
            this.textBoxAdln = new System.Windows.Forms.TextBox();
            this.textBoxAdem = new System.Windows.Forms.TextBox();
            this.Adbtn2 = new System.Windows.Forms.Button();
            this.AdBtnClear = new System.Windows.Forms.Button();
            this.AdLogout = new System.Windows.Forms.Button();
            this.Carinst = new System.Windows.Forms.ComboBox();
            this.Assigncar = new System.Windows.Forms.Button();
            this.adminTableAdapter = new INTRO_USERS.AdminDataTableAdapters.adminTableAdapter();
            this.adminData = new INTRO_USERS.AdminData();
            this.CarData = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Refresh = new System.Windows.Forms.Button();
            this.Instlink = new System.Windows.Forms.Button();
            this.pointlist = new System.Windows.Forms.ListBox();
            this.points = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.car = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.adminData)).BeginInit();
            this.SuspendLayout();
            // 
            // Adlabel1
            // 
            this.Adlabel1.AutoSize = true;
            this.Adlabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel1.Location = new System.Drawing.Point(163, 32);
            this.Adlabel1.Name = "Adlabel1";
            this.Adlabel1.Size = new System.Drawing.Size(326, 39);
            this.Adlabel1.TabIndex = 0;
            this.Adlabel1.Text = "Admin Control Page";
            // 
            // Adlabel2
            // 
            this.Adlabel2.AutoSize = true;
            this.Adlabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel2.Location = new System.Drawing.Point(55, 113);
            this.Adlabel2.Name = "Adlabel2";
            this.Adlabel2.Size = new System.Drawing.Size(176, 20);
            this.Adlabel2.TabIndex = 1;
            this.Adlabel2.Text = "Register New Instructor";
            // 
            // Adlabel3
            // 
            this.Adlabel3.AutoSize = true;
            this.Adlabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel3.Location = new System.Drawing.Point(336, 113);
            this.Adlabel3.Name = "Adlabel3";
            this.Adlabel3.Size = new System.Drawing.Size(153, 20);
            this.Adlabel3.TabIndex = 2;
            this.Adlabel3.Text = "Register New Admin";
            // 
            // textBoxInuse
            // 
            this.textBoxInuse.Location = new System.Drawing.Point(130, 163);
            this.textBoxInuse.Name = "textBoxInuse";
            this.textBoxInuse.Size = new System.Drawing.Size(126, 20);
            this.textBoxInuse.TabIndex = 3;
            // 
            // textBoxInpass
            // 
            this.textBoxInpass.Location = new System.Drawing.Point(130, 204);
            this.textBoxInpass.Name = "textBoxInpass";
            this.textBoxInpass.Size = new System.Drawing.Size(126, 20);
            this.textBoxInpass.TabIndex = 4;
            // 
            // textBoxInfn
            // 
            this.textBoxInfn.Location = new System.Drawing.Point(130, 243);
            this.textBoxInfn.Name = "textBoxInfn";
            this.textBoxInfn.Size = new System.Drawing.Size(126, 20);
            this.textBoxInfn.TabIndex = 5;
            // 
            // textBoxInln
            // 
            this.textBoxInln.Location = new System.Drawing.Point(130, 288);
            this.textBoxInln.Name = "textBoxInln";
            this.textBoxInln.Size = new System.Drawing.Size(126, 20);
            this.textBoxInln.TabIndex = 6;
            // 
            // textBoxInph
            // 
            this.textBoxInph.Location = new System.Drawing.Point(130, 371);
            this.textBoxInph.Name = "textBoxInph";
            this.textBoxInph.Size = new System.Drawing.Size(126, 20);
            this.textBoxInph.TabIndex = 7;
            // 
            // textBoxInem
            // 
            this.textBoxInem.Location = new System.Drawing.Point(130, 331);
            this.textBoxInem.Name = "textBoxInem";
            this.textBoxInem.Size = new System.Drawing.Size(126, 20);
            this.textBoxInem.TabIndex = 8;
            // 
            // AdBtn1
            // 
            this.AdBtn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdBtn1.Location = new System.Drawing.Point(59, 422);
            this.AdBtn1.Name = "AdBtn1";
            this.AdBtn1.Size = new System.Drawing.Size(197, 46);
            this.AdBtn1.TabIndex = 9;
            this.AdBtn1.Text = "Create New Instructor";
            this.AdBtn1.UseVisualStyleBackColor = true;
            this.AdBtn1.Click += new System.EventHandler(this.AdBtn1_Click);
            // 
            // Adlabel4
            // 
            this.Adlabel4.AutoSize = true;
            this.Adlabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel4.Location = new System.Drawing.Point(34, 161);
            this.Adlabel4.Name = "Adlabel4";
            this.Adlabel4.Size = new System.Drawing.Size(85, 20);
            this.Adlabel4.TabIndex = 10;
            this.Adlabel4.Text = "UserName";
            // 
            // Adlabel5
            // 
            this.Adlabel5.AutoSize = true;
            this.Adlabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel5.Location = new System.Drawing.Point(38, 204);
            this.Adlabel5.Name = "Adlabel5";
            this.Adlabel5.Size = new System.Drawing.Size(82, 20);
            this.Adlabel5.TabIndex = 11;
            this.Adlabel5.Text = "PassWord";
            // 
            // Adlabel6
            // 
            this.Adlabel6.AutoSize = true;
            this.Adlabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel6.Location = new System.Drawing.Point(38, 243);
            this.Adlabel6.Name = "Adlabel6";
            this.Adlabel6.Size = new System.Drawing.Size(86, 20);
            this.Adlabel6.TabIndex = 12;
            this.Adlabel6.Text = "First Name";
            // 
            // Adlabel7
            // 
            this.Adlabel7.AutoSize = true;
            this.Adlabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel7.Location = new System.Drawing.Point(38, 288);
            this.Adlabel7.Name = "Adlabel7";
            this.Adlabel7.Size = new System.Drawing.Size(86, 20);
            this.Adlabel7.TabIndex = 13;
            this.Adlabel7.Text = "Last Name";
            // 
            // Adlabel8
            // 
            this.Adlabel8.AutoSize = true;
            this.Adlabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel8.Location = new System.Drawing.Point(38, 329);
            this.Adlabel8.Name = "Adlabel8";
            this.Adlabel8.Size = new System.Drawing.Size(48, 20);
            this.Adlabel8.TabIndex = 14;
            this.Adlabel8.Text = "Email";
            // 
            // Adlabel9
            // 
            this.Adlabel9.AutoSize = true;
            this.Adlabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel9.Location = new System.Drawing.Point(38, 371);
            this.Adlabel9.Name = "Adlabel9";
            this.Adlabel9.Size = new System.Drawing.Size(55, 20);
            this.Adlabel9.TabIndex = 15;
            this.Adlabel9.Text = "Phone";
            // 
            // Adlabel10
            // 
            this.Adlabel10.AutoSize = true;
            this.Adlabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel10.Location = new System.Drawing.Point(320, 161);
            this.Adlabel10.Name = "Adlabel10";
            this.Adlabel10.Size = new System.Drawing.Size(85, 20);
            this.Adlabel10.TabIndex = 16;
            this.Adlabel10.Text = "UserName";
            // 
            // Adlabel11
            // 
            this.Adlabel11.AutoSize = true;
            this.Adlabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel11.Location = new System.Drawing.Point(320, 202);
            this.Adlabel11.Name = "Adlabel11";
            this.Adlabel11.Size = new System.Drawing.Size(82, 20);
            this.Adlabel11.TabIndex = 17;
            this.Adlabel11.Text = "PassWord";
            // 
            // Adlabel12
            // 
            this.Adlabel12.AutoSize = true;
            this.Adlabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel12.Location = new System.Drawing.Point(320, 241);
            this.Adlabel12.Name = "Adlabel12";
            this.Adlabel12.Size = new System.Drawing.Size(86, 20);
            this.Adlabel12.TabIndex = 18;
            this.Adlabel12.Text = "First Name";
            // 
            // Adlabel13
            // 
            this.Adlabel13.AutoSize = true;
            this.Adlabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel13.Location = new System.Drawing.Point(316, 288);
            this.Adlabel13.Name = "Adlabel13";
            this.Adlabel13.Size = new System.Drawing.Size(86, 20);
            this.Adlabel13.TabIndex = 19;
            this.Adlabel13.Text = "Last Name";
            // 
            // Adlabel14
            // 
            this.Adlabel14.AutoSize = true;
            this.Adlabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adlabel14.Location = new System.Drawing.Point(316, 328);
            this.Adlabel14.Name = "Adlabel14";
            this.Adlabel14.Size = new System.Drawing.Size(48, 20);
            this.Adlabel14.TabIndex = 20;
            this.Adlabel14.Text = "Email";
            // 
            // textBoxAduse
            // 
            this.textBoxAduse.Location = new System.Drawing.Point(412, 163);
            this.textBoxAduse.Name = "textBoxAduse";
            this.textBoxAduse.Size = new System.Drawing.Size(100, 20);
            this.textBoxAduse.TabIndex = 21;
            // 
            // textBoxAdpass
            // 
            this.textBoxAdpass.Location = new System.Drawing.Point(412, 203);
            this.textBoxAdpass.Name = "textBoxAdpass";
            this.textBoxAdpass.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdpass.TabIndex = 22;
            // 
            // textBoxAdfn
            // 
            this.textBoxAdfn.Location = new System.Drawing.Point(413, 242);
            this.textBoxAdfn.Name = "textBoxAdfn";
            this.textBoxAdfn.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdfn.TabIndex = 23;
            // 
            // textBoxAdln
            // 
            this.textBoxAdln.Location = new System.Drawing.Point(413, 288);
            this.textBoxAdln.Name = "textBoxAdln";
            this.textBoxAdln.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdln.TabIndex = 24;
            // 
            // textBoxAdem
            // 
            this.textBoxAdem.Location = new System.Drawing.Point(413, 328);
            this.textBoxAdem.Name = "textBoxAdem";
            this.textBoxAdem.Size = new System.Drawing.Size(100, 20);
            this.textBoxAdem.TabIndex = 25;
            // 
            // Adbtn2
            // 
            this.Adbtn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adbtn2.Location = new System.Drawing.Point(393, 422);
            this.Adbtn2.Name = "Adbtn2";
            this.Adbtn2.Size = new System.Drawing.Size(197, 46);
            this.Adbtn2.TabIndex = 26;
            this.Adbtn2.Text = "Create New Admin";
            this.Adbtn2.UseVisualStyleBackColor = true;
            this.Adbtn2.Click += new System.EventHandler(this.Adbtn2_Click);
            // 
            // AdBtnClear
            // 
            this.AdBtnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdBtnClear.Location = new System.Drawing.Point(293, 422);
            this.AdBtnClear.Name = "AdBtnClear";
            this.AdBtnClear.Size = new System.Drawing.Size(75, 46);
            this.AdBtnClear.TabIndex = 27;
            this.AdBtnClear.Text = "Clear";
            this.AdBtnClear.UseVisualStyleBackColor = true;
            this.AdBtnClear.Click += new System.EventHandler(this.AdBtnClear_Click);
            // 
            // AdLogout
            // 
            this.AdLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdLogout.Location = new System.Drawing.Point(293, 490);
            this.AdLogout.Name = "AdLogout";
            this.AdLogout.Size = new System.Drawing.Size(75, 32);
            this.AdLogout.TabIndex = 28;
            this.AdLogout.Text = "LogOut";
            this.AdLogout.UseVisualStyleBackColor = true;
            this.AdLogout.Click += new System.EventHandler(this.AdLogout_Click);
            // 
            // Carinst
            // 
            this.Carinst.FormattingEnabled = true;
            this.Carinst.Location = new System.Drawing.Point(628, 72);
            this.Carinst.Name = "Carinst";
            this.Carinst.Size = new System.Drawing.Size(169, 21);
            this.Carinst.TabIndex = 29;
            this.Carinst.SelectedIndexChanged += new System.EventHandler(this.Carinst_SelectedIndexChanged);
            // 
            // Assigncar
            // 
            this.Assigncar.Location = new System.Drawing.Point(628, 99);
            this.Assigncar.Name = "Assigncar";
            this.Assigncar.Size = new System.Drawing.Size(169, 46);
            this.Assigncar.TabIndex = 32;
            this.Assigncar.Text = "Assign Car";
            this.Assigncar.UseVisualStyleBackColor = true;
            this.Assigncar.Click += new System.EventHandler(this.Assigncar_Click);
            // 
            // adminTableAdapter
            // 
            this.adminTableAdapter.ClearBeforeFill = true;
            // 
            // adminData
            // 
            this.adminData.DataSetName = "AdminData";
            this.adminData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CarData
            // 
            this.CarData.FormatString = "D";
            this.CarData.FormattingEnabled = true;
            this.CarData.Location = new System.Drawing.Point(683, 269);
            this.CarData.Name = "CarData";
            this.CarData.Size = new System.Drawing.Size(404, 199);
            this.CarData.TabIndex = 34;
            this.CarData.SelectedIndexChanged += new System.EventHandler(this.CarData_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(720, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "Cars Assigned This Week";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Refresh
            // 
            this.Refresh.Location = new System.Drawing.Point(683, 509);
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(139, 46);
            this.Refresh.TabIndex = 36;
            this.Refresh.Text = "Refresh";
            this.Refresh.UseVisualStyleBackColor = true;
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // Instlink
            // 
            this.Instlink.Location = new System.Drawing.Point(628, 161);
            this.Instlink.Name = "Instlink";
            this.Instlink.Size = new System.Drawing.Size(169, 46);
            this.Instlink.TabIndex = 37;
            this.Instlink.Text = "Instructor View";
            this.Instlink.UseVisualStyleBackColor = true;
            this.Instlink.Click += new System.EventHandler(this.Instlink_Click_1);
            // 
            // pointlist
            // 
            this.pointlist.FormattingEnabled = true;
            this.pointlist.Location = new System.Drawing.Point(1106, 59);
            this.pointlist.Name = "pointlist";
            this.pointlist.Size = new System.Drawing.Size(392, 212);
            this.pointlist.TabIndex = 38;
            // 
            // points
            // 
            this.points.AutoSize = true;
            this.points.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.points.Location = new System.Drawing.Point(1255, 32);
            this.points.Name = "points";
            this.points.Size = new System.Drawing.Size(108, 20);
            this.points.TabIndex = 39;
            this.points.Text = "Appointments";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(824, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 46);
            this.button1.TabIndex = 40;
            this.button1.Text = "Cancel car";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // car
            // 
            this.car.FormattingEnabled = true;
            this.car.Location = new System.Drawing.Point(824, 72);
            this.car.Name = "car";
            this.car.Size = new System.Drawing.Size(169, 21);
            this.car.TabIndex = 30;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(628, 32);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(169, 20);
            this.dateTimePicker1.TabIndex = 41;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(683, 483);
            this.dateTimePicker2.MinDate = new System.DateTime(2017, 6, 12, 0, 0, 0, 0);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(102, 20);
            this.dateTimePicker2.TabIndex = 42;
            // 
            // Adminstuff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1507, 615);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.points);
            this.Controls.Add(this.pointlist);
            this.Controls.Add(this.Instlink);
            this.Controls.Add(this.Refresh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CarData);
            this.Controls.Add(this.Assigncar);
            this.Controls.Add(this.car);
            this.Controls.Add(this.Carinst);
            this.Controls.Add(this.AdLogout);
            this.Controls.Add(this.AdBtnClear);
            this.Controls.Add(this.Adbtn2);
            this.Controls.Add(this.textBoxAdem);
            this.Controls.Add(this.textBoxAdln);
            this.Controls.Add(this.textBoxAdfn);
            this.Controls.Add(this.textBoxAdpass);
            this.Controls.Add(this.textBoxAduse);
            this.Controls.Add(this.Adlabel14);
            this.Controls.Add(this.Adlabel13);
            this.Controls.Add(this.Adlabel12);
            this.Controls.Add(this.Adlabel11);
            this.Controls.Add(this.Adlabel10);
            this.Controls.Add(this.Adlabel9);
            this.Controls.Add(this.Adlabel8);
            this.Controls.Add(this.Adlabel7);
            this.Controls.Add(this.Adlabel6);
            this.Controls.Add(this.Adlabel5);
            this.Controls.Add(this.Adlabel4);
            this.Controls.Add(this.AdBtn1);
            this.Controls.Add(this.textBoxInem);
            this.Controls.Add(this.textBoxInph);
            this.Controls.Add(this.textBoxInln);
            this.Controls.Add(this.textBoxInfn);
            this.Controls.Add(this.textBoxInpass);
            this.Controls.Add(this.textBoxInuse);
            this.Controls.Add(this.Adlabel3);
            this.Controls.Add(this.Adlabel2);
            this.Controls.Add(this.Adlabel1);
            this.Name = "Adminstuff";
            this.Text = "7";
            ((System.ComponentModel.ISupportInitialize)(this.adminData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Adlabel1;
        private System.Windows.Forms.Label Adlabel2;
        private System.Windows.Forms.Label Adlabel3;
        private System.Windows.Forms.TextBox textBoxInuse;
        private System.Windows.Forms.TextBox textBoxInpass;
        private System.Windows.Forms.TextBox textBoxInfn;
        private System.Windows.Forms.TextBox textBoxInln;
        private System.Windows.Forms.TextBox textBoxInph;
        private System.Windows.Forms.TextBox textBoxInem;
        private System.Windows.Forms.Button AdBtn1;
        private System.Windows.Forms.Label Adlabel4;
        private System.Windows.Forms.Label Adlabel5;
        private System.Windows.Forms.Label Adlabel6;
        private System.Windows.Forms.Label Adlabel7;
        private System.Windows.Forms.Label Adlabel8;
        private System.Windows.Forms.Label Adlabel9;
        private System.Windows.Forms.Label Adlabel10;
        private System.Windows.Forms.Label Adlabel11;
        private System.Windows.Forms.Label Adlabel12;
        private System.Windows.Forms.Label Adlabel13;
        private System.Windows.Forms.Label Adlabel14;
        private System.Windows.Forms.TextBox textBoxAduse;
        private System.Windows.Forms.TextBox textBoxAdpass;
        private System.Windows.Forms.TextBox textBoxAdfn;
        private System.Windows.Forms.TextBox textBoxAdln;
        private System.Windows.Forms.TextBox textBoxAdem;
        private System.Windows.Forms.Button Adbtn2;
        private System.Windows.Forms.Button AdBtnClear;
        private System.Windows.Forms.Button AdLogout;
        private System.Windows.Forms.ComboBox Carinst;
        private System.Windows.Forms.Button Assigncar;
        private AdminDataTableAdapters.adminTableAdapter adminTableAdapter;
        private AdminData adminData;
        private System.Windows.Forms.ListBox CarData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Refresh;
        private System.Windows.Forms.Button Instlink;
        private System.Windows.Forms.ListBox pointlist;
        private System.Windows.Forms.Label points;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox car;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}