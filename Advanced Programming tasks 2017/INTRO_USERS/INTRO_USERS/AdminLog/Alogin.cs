﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace INTRO_USERS
{
    public partial class Alogin : Form
    {
        public Alogin()
        {
            InitializeComponent();
            textBoxAdminpass.PasswordChar = '*';
            textBoxAdminpass.MaxLength = 20;  
            }

        private void Adminuse_TextChanged(object sender, EventArgs e)
        {
        }

        
       
         public static SqlConnection con = new SqlConnection(@"Data Source=Localhost;Database=D2;Integrated Security=True");
        /// <summary>
        /// This is the first method called when the program form loads.
        /// </summary>

        //This line of code allows us to obscure the password visually and limit the max chars in textbox
        //max textbox character count



        /// <summary>
        /// Clicked when user decides they are ready to log in, 
        /// Will get username and password, use that to query database and check that username and password are correct.
        /// A message box will be used to state whether or not we logged in successfully
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Adminsub_Click(object sender, EventArgs e)
        {
            //Variables to be used: 1x bool, 4x string
            bool loggedIn = false;
            string username = "", firstname = "", lastname = "", password = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(textBoxAdminuse.Text.Trim()) || "".Equals(textBoxAdminpass.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                /*YOUR CODE HERE*/
                username = textBoxAdminuse.Text.Trim();
                password = textBoxAdminpass.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/





            SQL.selectQuery("Select * from admin");



            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[1].ToString();
                        lastname = SQL.read[2].ToString();
                        break;
                    }
                }
            }



            else
            {
                MessageBox.Show("No Admin Users Detected");
                return;
            }



            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: " + username+"'");
                initialiseTextBoxes();

                Hide();
                Adminstuff Adminstuff = new Adminstuff();
                Adminstuff.ShowDialog();
                this.Close();
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxAdminuse.Focus();
                return;
            }
        }



        private void Alogclear_Click(object sender, EventArgs e)
        {
            initialiseTextBoxes();
        } 
        

        /// <summary>
        /// Initialises all textboxes to blank text
        /// Re focus to first text box
        /// </summary>
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            textBoxAdminuse.Focus();
        }

        private void Rturnadmin_Click(object sender, EventArgs e)
        {
            Hide();
           LoginPage login = new LoginPage();
           login.ShowDialog();
           this.Close();
        }

        
    }
}
