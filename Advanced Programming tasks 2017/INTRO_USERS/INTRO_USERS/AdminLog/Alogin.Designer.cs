﻿namespace INTRO_USERS
{
    partial class Alogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alogin));
            this.textBoxAdminuse = new System.Windows.Forms.TextBox();
            this.textBoxAdminpass = new System.Windows.Forms.TextBox();
            this.Adminsub = new System.Windows.Forms.Button();
            this.Adminlabel1 = new System.Windows.Forms.Label();
            this.Adminlabel2 = new System.Windows.Forms.Label();
            this.Adminlabel3 = new System.Windows.Forms.Label();
            this.Rturnadmin = new System.Windows.Forms.Button();
            this.Alogclear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxAdminuse
            // 
            this.textBoxAdminuse.Location = new System.Drawing.Point(440, 146);
            this.textBoxAdminuse.Name = "textBoxAdminuse";
            this.textBoxAdminuse.Size = new System.Drawing.Size(109, 20);
            this.textBoxAdminuse.TabIndex = 0;
            this.textBoxAdminuse.TextChanged += new System.EventHandler(this.Adminuse_TextChanged);
            // 
            // textBoxAdminpass
            // 
            this.textBoxAdminpass.Location = new System.Drawing.Point(440, 183);
            this.textBoxAdminpass.Name = "textBoxAdminpass";
            this.textBoxAdminpass.Size = new System.Drawing.Size(109, 20);
            this.textBoxAdminpass.TabIndex = 1;
            // 
            // Adminsub
            // 
            this.Adminsub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adminsub.Location = new System.Drawing.Point(317, 224);
            this.Adminsub.Name = "Adminsub";
            this.Adminsub.Size = new System.Drawing.Size(232, 33);
            this.Adminsub.TabIndex = 2;
            this.Adminsub.Text = "Submit";
            this.Adminsub.UseVisualStyleBackColor = true;
            this.Adminsub.Click += new System.EventHandler(this.Adminsub_Click);
            // 
            // Adminlabel1
            // 
            this.Adminlabel1.AutoSize = true;
            this.Adminlabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adminlabel1.Location = new System.Drawing.Point(313, 144);
            this.Adminlabel1.Name = "Adminlabel1";
            this.Adminlabel1.Size = new System.Drawing.Size(85, 20);
            this.Adminlabel1.TabIndex = 3;
            this.Adminlabel1.Text = "UserName";
            // 
            // Adminlabel2
            // 
            this.Adminlabel2.AutoSize = true;
            this.Adminlabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adminlabel2.Location = new System.Drawing.Point(313, 181);
            this.Adminlabel2.Name = "Adminlabel2";
            this.Adminlabel2.Size = new System.Drawing.Size(78, 20);
            this.Adminlabel2.TabIndex = 4;
            this.Adminlabel2.Text = "Password";
            // 
            // Adminlabel3
            // 
            this.Adminlabel3.AutoSize = true;
            this.Adminlabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Adminlabel3.Location = new System.Drawing.Point(314, 84);
            this.Adminlabel3.Name = "Adminlabel3";
            this.Adminlabel3.Size = new System.Drawing.Size(209, 39);
            this.Adminlabel3.TabIndex = 5;
            this.Adminlabel3.Text = "Admin Login";
            this.Adminlabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Rturnadmin
            // 
            this.Rturnadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rturnadmin.Location = new System.Drawing.Point(317, 303);
            this.Rturnadmin.Name = "Rturnadmin";
            this.Rturnadmin.Size = new System.Drawing.Size(232, 34);
            this.Rturnadmin.TabIndex = 6;
            this.Rturnadmin.Text = "Back to Login";
            this.Rturnadmin.UseVisualStyleBackColor = true;
            this.Rturnadmin.Click += new System.EventHandler(this.Rturnadmin_Click);
            // 
            // Alogclear
            // 
            this.Alogclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Alogclear.Location = new System.Drawing.Point(317, 263);
            this.Alogclear.Name = "Alogclear";
            this.Alogclear.Size = new System.Drawing.Size(232, 34);
            this.Alogclear.TabIndex = 7;
            this.Alogclear.Text = "Clear";
            this.Alogclear.UseVisualStyleBackColor = true;
            this.Alogclear.Click += new System.EventHandler(this.Alogclear_Click);
            // 
            // Alogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(823, 532);
            this.Controls.Add(this.Alogclear);
            this.Controls.Add(this.Rturnadmin);
            this.Controls.Add(this.Adminlabel3);
            this.Controls.Add(this.Adminlabel2);
            this.Controls.Add(this.Adminlabel1);
            this.Controls.Add(this.Adminsub);
            this.Controls.Add(this.textBoxAdminpass);
            this.Controls.Add(this.textBoxAdminuse);
            this.Name = "Alogin";
            this.Text = "Alogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxAdminuse;
        private System.Windows.Forms.TextBox textBoxAdminpass;
        private System.Windows.Forms.Button Adminsub;
        private System.Windows.Forms.Label Adminlabel1;
        private System.Windows.Forms.Label Adminlabel2;
        private System.Windows.Forms.Label Adminlabel3;
        private System.Windows.Forms.Button Rturnadmin;
        private System.Windows.Forms.Button Alogclear;
    }
}