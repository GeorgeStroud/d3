﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace INTRO_USERS
{
    public partial class Adminstuff : Form
    {
        public Adminstuff()
        {
            InitializeComponent();

            SQL.selectQuery("Select * from instructor");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Carinst.Items.Add(SQL.read[0]);
                }
            }
            SQL.selectQuery($"SELECT * from Car");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    car.Items.Add(SQL.read[0]);
                }
            }



            List<String> CarDatae = new List<String>();

            SQL.selectQuery("SELECT * FROM assigned");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    CarDatae.Add(" username: " + SQL.read[1].ToString()  + "     licence: "+ SQL.read[2].ToString());

                }
            }

            CarData.DataSource = CarDatae;
            
                        

        }

        private void AdBtn1_Click(object sender, EventArgs e)
        {
            //variables to be used
            string username = "", password = "", fname = "", sname = "", email = "", phone = "";

            //Check that the text boxes has something typed in it using a method



            //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
            try
            {
                username = textBoxInuse.Text.Trim();
                password = textBoxInpass.Text.Trim();
                fname = textBoxInfn.Text.Trim();
                sname = textBoxInln.Text.Trim();
                email = textBoxInem.Text.Trim();
                phone = textBoxInph.Text.Trim();

            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }

            //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
            //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
            //      then copy across the statement and where there are string replace the actual text for the variables stored above.
            //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
            try
            {
                SQL.executeQuery("INSERT INTO Instructor Values ('" + username + "','" + password + "','" + fname + "','" + sname + "','" + email + "','" + phone + "')");
                MessageBox.Show("Successfully Registered: " + fname + " " + sname + ". Your username is: " + username);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }



            //success message for the user to know it worked
           
        }

        /// <summary>
        /// Clears all text boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdBtnClear_Click(object sender, EventArgs e)

        {
            initialiseTextBoxes();
        }

        /// <summary>
        /// Initialises all textboxes to blank text
        /// Re focus to first text box
        /// </summary>
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            textBoxInuse.Focus();
        }

       


        private void Adbtn2_Click(object sender, EventArgs e)
        {
            {
                //variables to be used
                string username = "", password = "", fname = "", sname = "", email = "";

                //Check that the text boxes has something typed in it using a method

               

                //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
                try
                {
                    username = textBoxAduse.Text.Trim();
                    password = textBoxAdpass.Text.Trim();
                    fname = textBoxAdfn.Text.Trim();
                    sname = textBoxAdln.Text.Trim();
                    email = textBoxAdem.Text.Trim();


                }
                catch
                {
                    //Error message, more useful when you are storing numbers etc. into the database.
                    MessageBox.Show("Please make sure your text is in correct format.");
                    return;
                }

                //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
                //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
                //      then copy across the statement and where there are string replace the actual text for the variables stored above.
                //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
                try
                {
                    SQL.executeQuery("INSERT INTO Admin Values ('" + username + "','" + password + "','" + fname + "','" + sname + "','" + email + "')");

                    MessageBox.Show("Successfully Registered: " + fname + " " + sname + ". Your username is: " + username);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                    return;
                }



                //success message for the user to know it worked
               

                //Go back to the login page since we registered successfully to let the user log in
                //closes the register form that was hidden
            }

            /// <summary>
            /// Clears all text boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>


                

            


            {

            }
        }

        private void AdLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Assigncar_Click(object sender, EventArgs e)
        {
            string username = "", licence = "", hirage = "";

            //Check that the text boxes has something typed in it using a method



            //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
            try
            {
                username = Carinst.Text.Trim();
                licence = car.Text.Trim();
                hirage = dateTimePicker1.Text.Trim();
                
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please make sure your text is in correct format.");
                return;
            }
            try
            {
                SQL.executeQuery("INSERT INTO Assigned Values ('" + username + "','" + licence + "','" + hirage + "')");

                MessageBox.Show("Successfully Registered: " + username + " with car  " + licence + "");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }

        }
        
        


        private void Carinst_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Refresh_Click(object sender, EventArgs e)
        {

            List<String> CarDatae = new List<String>();

            SQL.selectQuery("SELECT * FROM assigned WHERE Date='"+dateTimePicker2.Text+"'");


            if (SQL.read.HasRows)
            {

                

                    while (SQL.read.Read())
                    {
                        CarDatae.Add(" username: " + SQL.read[0].ToString() + "     licence: " + SQL.read[1].ToString() + "  Date: "+SQL.read[2].ToString());

                    }
                
            }

            CarData.DataSource = CarDatae;


            List<String> pointers = new List<String>();

            SQL.selectQuery("SELECT * FROM appointment");


            if (SQL.read.HasRows)
            {



                while (SQL.read.Read())
                {
                    pointers.Add(" username: " + SQL.read[1].ToString() + "     licence: " + SQL.read[2].ToString());

                }

            }

            pointlist.DataSource = pointers;
        }

       


        private void label1_Click(object sender, EventArgs e)
        {

        }

        public static string formatToSQLDate(DateTime myDate)
        {
            return myDate.Year.ToString() + "-" + myDate.Month.ToString() + "-" + myDate.Day.ToString();
        }

        private void CarData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Instlink_Click(object sender, EventArgs e)
        {
            Hide();
            Instructorslot Instructorslot = new Instructorslot();
            Instructorslot.ShowDialog();
            this.Close();
        }

        private void Instlink_Click_1(object sender, EventArgs e)
        {
            Hide();
            Instructorslot Instructorslot = new Instructorslot();
            Instructorslot.ShowDialog();
            this.Close();
        }


    }
}
   


       
        
    
    




