﻿namespace INTRO_USERS
{
    partial class Instructorslot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Instructorslot));
            this.button1 = new System.Windows.Forms.Button();
            this.instcom = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnlog = new System.Windows.Forms.Button();
            this.Refresh = new System.Windows.Forms.Button();
            this.slot = new System.Windows.Forms.ListBox();
            this.Verdate = new System.Windows.Forms.DateTimePicker();
            this.datesel = new System.Windows.Forms.Button();
            this.Specapp = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.delinst = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 45);
            this.button1.TabIndex = 0;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // instcom
            // 
            this.instcom.FormattingEnabled = true;
            this.instcom.Location = new System.Drawing.Point(51, 13);
            this.instcom.Name = "instcom";
            this.instcom.Size = new System.Drawing.Size(298, 21);
            this.instcom.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "MM-dd-yyyy ";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(220, 78);
            this.dateTimePicker1.MinDate = new System.DateTime(2017, 5, 21, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker1.TabIndex = 4;
            this.dateTimePicker1.Value = new System.DateTime(2017, 6, 10, 0, 0, 0, 0);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(51, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(298, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // btnlog
            // 
            this.btnlog.Location = new System.Drawing.Point(51, 511);
            this.btnlog.Name = "btnlog";
            this.btnlog.Size = new System.Drawing.Size(176, 63);
            this.btnlog.TabIndex = 7;
            this.btnlog.Text = "LogOut";
            this.btnlog.UseVisualStyleBackColor = true;
            this.btnlog.Click += new System.EventHandler(this.btnlog_Click);
            // 
            // Refresh
            // 
            this.Refresh.Location = new System.Drawing.Point(99, 184);
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(250, 45);
            this.Refresh.TabIndex = 8;
            this.Refresh.Text = "Refresh";
            this.Refresh.UseVisualStyleBackColor = true;
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // slot
            // 
            this.slot.FormattingEnabled = true;
            this.slot.Location = new System.Drawing.Point(404, 13);
            this.slot.Name = "slot";
            this.slot.Size = new System.Drawing.Size(390, 381);
            this.slot.TabIndex = 10;
            // 
            // Verdate
            // 
            this.Verdate.CustomFormat = "MM-dd-yyyy";
            this.Verdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Verdate.Location = new System.Drawing.Point(987, 406);
            this.Verdate.Name = "Verdate";
            this.Verdate.Size = new System.Drawing.Size(92, 20);
            this.Verdate.TabIndex = 11;
            // 
            // datesel
            // 
            this.datesel.Location = new System.Drawing.Point(933, 432);
            this.datesel.Name = "datesel";
            this.datesel.Size = new System.Drawing.Size(180, 50);
            this.datesel.TabIndex = 12;
            this.datesel.Text = "Select a Date";
            this.datesel.UseVisualStyleBackColor = true;
            this.datesel.Click += new System.EventHandler(this.datesel_Click);
            // 
            // Specapp
            // 
            this.Specapp.FormattingEnabled = true;
            this.Specapp.Location = new System.Drawing.Point(815, 13);
            this.Specapp.Name = "Specapp";
            this.Specapp.Size = new System.Drawing.Size(381, 381);
            this.Specapp.TabIndex = 13;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(646, 402);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // delinst
            // 
            this.delinst.FormattingEnabled = true;
            this.delinst.Location = new System.Drawing.Point(494, 403);
            this.delinst.Name = "delinst";
            this.delinst.Size = new System.Drawing.Size(129, 21);
            this.delinst.TabIndex = 15;
            this.delinst.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // Instructorslot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1319, 612);
            this.Controls.Add(this.delinst);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Specapp);
            this.Controls.Add(this.datesel);
            this.Controls.Add(this.Verdate);
            this.Controls.Add(this.slot);
            this.Controls.Add(this.Refresh);
            this.Controls.Add(this.btnlog);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.instcom);
            this.Controls.Add(this.button1);
            this.Name = "Instructorslot";
            this.Text = "Instructorslot";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox instcom;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnlog;
        private System.Windows.Forms.Button Refresh;
        private System.Windows.Forms.ListBox slot;
        private System.Windows.Forms.DateTimePicker Verdate;
        private System.Windows.Forms.Button datesel;
        private System.Windows.Forms.ListBox Specapp;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox delinst;
    }
}