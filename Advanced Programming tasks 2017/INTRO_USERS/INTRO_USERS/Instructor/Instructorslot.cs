﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace INTRO_USERS
{
    public partial class Instructorslot : Form
    {
        public Instructorslot()
        {
            InitializeComponent();

            SQL.selectQuery("Select * from instructor");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    instcom.Items.Add(SQL.read[0]);
                }
            }

            SQL.selectQuery("Select * from times");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    comboBox1.Items.Add(SQL.read[0]);
                }
            }

            SQL.selectQuery("Select * from timeslot");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    delinst.Items.Add(SQL.read[0]);
                }
            }


            List<String> Timeslotz = new List<String>();

            SQL.selectQuery("SELECT * FROM timeslot");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Timeslotz.Add(" ID: " + SQL.read[0].ToString() + "     time: " + SQL.read[1].ToString() + "   date: " + SQL.read[2].ToString() + "     username: " + SQL.read[3].ToString());

                }
            }

            slot.DataSource = Timeslotz;

            List<String> valve = new List<String>();

            SQL.selectQuery("select * from timeslot where date='" + Verdate + "'");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    valve.Add(" ID: " + SQL.read[0].ToString() + "     time: " + SQL.read[1].ToString() + "   date: " + SQL.read[2].ToString() + "     username: " + SQL.read[3].ToString());

                }
            }

            Specapp.DataSource = valve;
        }

        private void Refresh_Click(object sender, EventArgs e)
        {


            List<String> Timeslotz = new List<String>();

            SQL.selectQuery("SELECT * FROM timeslot");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Timeslotz.Add(" ID: " + SQL.read[0].ToString() + "     time: " + SQL.read[1].ToString() + "   date: " + SQL.read[2].ToString() + "     username: " + SQL.read[3].ToString());

                }
            }

            slot.DataSource = Timeslotz;
        }

        private void btnlog_Click(object sender, EventArgs e)
        {
            this.Close();
        }






        private void button1_Click(object sender, EventArgs e)
        {
            {
                //variables to be used
                string time = "", date = "", username = "";

                //Check that the text boxes has something typed in it using a method



                //(1) GET the data from the textboxes and store into variables created above, good to put in a try catch with error message
                try
                {

                    time = comboBox1.Text.Trim();
                    date = dateTimePicker1.Text.Trim();
                    username = instcom.Text.Trim();


                }
                catch
                {
                    //Error message, more useful when you are storing numbers etc. into the database.
                    MessageBox.Show("Please make sure your text is in correct format.");
                    return;
                }

                //(2) Execute the INSERT statement, making sure all quotes and commas are in the correct places.
                //      Practice first on SQL Server Management Studio to make sure it is entering the correct data and in the correct format,
                //      then copy across the statement and where there are string replace the actual text for the variables stored above.
                //Example query: " INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1') "
                try
                {
                   

                    SQL.executeQuery("INSERT INTO timeslot Values ('" + time + "','" + date + "','" + username + "')");
                    MessageBox.Show("Successfully Registered: " + time + " " + date + "");
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sorry that did not work please try again.");
                    return;
                }

            }
        }

        /// <summary>
        /// Clears all text boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdBtnClear_Click(object sender, EventArgs e)

        {
            initialiseTextBoxes();
        }

        /// <summary>
        /// Initialises all textboxes to blank text
        /// Re focus to first text box
        /// </summary>
        void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }


                

            }

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void datesel_Click(object sender, EventArgs e)
        {
            List<String> valve = new List<String>();

            SQL.selectQuery("select * from timeslot where Date='" +Verdate.Text+ "'");


            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    valve.Add(" ID: " + SQL.read[0].ToString() + "     time: " + SQL.read[1].ToString() + "   date: " + SQL.read[2].ToString() + "     username: " + SQL.read[3].ToString());

                }
            }

            Specapp.DataSource = valve;
        }


    

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SQL.selectQuery("delete  from timeslot where id='" + delinst.Text + "'");
        }
    }
}

    


