﻿namespace INTRO_USERS
{
    partial class InstructorLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstructorLogin));
            this.Instructsub = new System.Windows.Forms.Button();
            this.textBoxInstuse = new System.Windows.Forms.TextBox();
            this.textBoxInstpass = new System.Windows.Forms.TextBox();
            this.Instlabel1 = new System.Windows.Forms.Label();
            this.Instlabel2 = new System.Windows.Forms.Label();
            this.Instretrn = new System.Windows.Forms.Button();
            this.InstClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Instructsub
            // 
            this.Instructsub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Instructsub.Location = new System.Drawing.Point(302, 229);
            this.Instructsub.Name = "Instructsub";
            this.Instructsub.Size = new System.Drawing.Size(256, 35);
            this.Instructsub.TabIndex = 0;
            this.Instructsub.Text = "Submit";
            this.Instructsub.UseVisualStyleBackColor = true;
            this.Instructsub.Click += new System.EventHandler(this.Instructsub_Click);
            // 
            // textBoxInstuse
            // 
            this.textBoxInstuse.Location = new System.Drawing.Point(411, 110);
            this.textBoxInstuse.Name = "textBoxInstuse";
            this.textBoxInstuse.Size = new System.Drawing.Size(147, 20);
            this.textBoxInstuse.TabIndex = 1;
            // 
            // textBoxInstpass
            // 
            this.textBoxInstpass.Location = new System.Drawing.Point(411, 169);
            this.textBoxInstpass.Name = "textBoxInstpass";
            this.textBoxInstpass.Size = new System.Drawing.Size(147, 20);
            this.textBoxInstpass.TabIndex = 2;
            // 
            // Instlabel1
            // 
            this.Instlabel1.AutoSize = true;
            this.Instlabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Instlabel1.Location = new System.Drawing.Point(315, 110);
            this.Instlabel1.Name = "Instlabel1";
            this.Instlabel1.Size = new System.Drawing.Size(85, 20);
            this.Instlabel1.TabIndex = 3;
            this.Instlabel1.Text = "UserName";
            // 
            // Instlabel2
            // 
            this.Instlabel2.AutoSize = true;
            this.Instlabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Instlabel2.Location = new System.Drawing.Point(316, 169);
            this.Instlabel2.Name = "Instlabel2";
            this.Instlabel2.Size = new System.Drawing.Size(82, 20);
            this.Instlabel2.TabIndex = 4;
            this.Instlabel2.Text = "PassWord";
            // 
            // Instretrn
            // 
            this.Instretrn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Instretrn.Location = new System.Drawing.Point(302, 321);
            this.Instretrn.Name = "Instretrn";
            this.Instretrn.Size = new System.Drawing.Size(255, 38);
            this.Instretrn.TabIndex = 5;
            this.Instretrn.Text = "Back To Login";
            this.Instretrn.UseVisualStyleBackColor = true;
            this.Instretrn.Click += new System.EventHandler(this.Instretrn_Click);
            // 
            // InstClear
            // 
            this.InstClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstClear.Location = new System.Drawing.Point(303, 271);
            this.InstClear.Name = "InstClear";
            this.InstClear.Size = new System.Drawing.Size(254, 44);
            this.InstClear.TabIndex = 6;
            this.InstClear.Text = "Clear";
            this.InstClear.UseVisualStyleBackColor = true;
            this.InstClear.Click += new System.EventHandler(this.InstClear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(320, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 39);
            this.label1.TabIndex = 7;
            this.label1.Text = "Instructor Login";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // InstructorLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(863, 495);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InstClear);
            this.Controls.Add(this.Instretrn);
            this.Controls.Add(this.Instlabel2);
            this.Controls.Add(this.Instlabel1);
            this.Controls.Add(this.textBoxInstpass);
            this.Controls.Add(this.textBoxInstuse);
            this.Controls.Add(this.Instructsub);
            this.Name = "InstructorLogin";
            this.Text = "InstructorLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Instructsub;
        private System.Windows.Forms.TextBox textBoxInstuse;
        private System.Windows.Forms.TextBox textBoxInstpass;
        private System.Windows.Forms.Label Instlabel1;
        private System.Windows.Forms.Label Instlabel2;
        private System.Windows.Forms.Button Instretrn;
        private System.Windows.Forms.Button InstClear;
        private System.Windows.Forms.Label label1;
    }
}