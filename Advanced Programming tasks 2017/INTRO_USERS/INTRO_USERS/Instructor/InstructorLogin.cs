﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace INTRO_USERS
{
    public partial class InstructorLogin : Form
    {
        public InstructorLogin()
        {
            InitializeComponent();
        }

        public static SqlConnection con = new SqlConnection(@"Data Source=Localhost;Database=D2;Integrated Security=True");
       
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Instructsub_Click(object sender, EventArgs e)
        {
           
            bool loggedIn = false;
            string username = "", firstname = "", lastname = "", password = "";

           
            if ("".Equals(textBoxInstuse.Text.Trim()) || "".Equals(textBoxInstpass.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            
            try
            {
               
                username = textBoxInstuse.Text.Trim();
                password = textBoxInstpass.Text.Trim();
            }
            catch
            {
               
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            





            SQL.selectQuery("Select * from instructor");



            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[1].ToString();
                        lastname = SQL.read[2].ToString();
                        break;
                    }
                }
            }



            else
            {
                MessageBox.Show("Not connecting");
                return;
            }



            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: '" + username+"'");
                initialiseTextBoxes();
                Hide();
                Instructorslot Instructorslot = new Instructorslot();
                Instructorslot.ShowDialog();
                this.Close();
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxInstuse.Focus();
                return;
            }
        }

        private void InstClear_Click(object sender, EventArgs e)
        {
            initialiseTextBoxes();
        }

        /// <summary>
        /// Initialises all textboxes to blank text
        /// Re focus to first text box
        /// </summary>
        public void initialiseTextBoxes()
        {
            //goes through and clears all of the textboxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //makes next place user types the text box
            textBoxInstuse.Focus();
        }




        private void Instretrn_Click(object sender, EventArgs e)
        {
            Hide();
            LoginPage LoginPage = new LoginPage();
            LoginPage.ShowDialog();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
