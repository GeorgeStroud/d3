create database D2
go
use D2
go

create table document(
Id int not null,
date int,
link varchar(20),
type varchar(20),
primary key (Id),
)

create table type(
name varchar(20) not null,
cost int,
hours int,
primary key(name)
)

create table instructor(
Iusername varchar(20) not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
phone int,
primary key(Iusername)
)

create table car(
licence varchar(20) not null,
make varchar(20),
primary key (licence),
)

create table assigned(
Iusername varchar(20) not null,
licence varchar(20) not null,
date date,
Foreign key (Iusername) references instructor,
Foreign key (licence) references car
)
drop table assigned
select * from assigned

create table admin(
username varchar(20) not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
primary key(username)
)

create table timeslot(
Id int not null IDENTITY(1,1),
time varchar(20) not null,
date varchar (20) not null,
Iusername varchar(20),
primary key(Id),
Foreign key (Iusername) references instructor
)

create table client(

username varchar(20)not null,
password varchar(20),
fname varchar(20),
sname varchar(20),
email varchar(20),
phone varchar(20),
name varchar(20),
Id int,
primary key (username),
Foreign Key (name) references type,
Foreign Key (Id) references document
)


create table appointment(
Aid int  not null IDENTITY(1,1),
username varchar(20),
Id int,
time varchar(20) not null,
date varchar (20) not null,
Iusername varchar(20),
primary key (username),
Foreign Key (username) references client,
Foreign Key (Id) references timeslot,
Foreign Key(Iusername) references Instructor
)

create table times
(
Id varchar(20),
primary key (Id)
)

insert into document values (1,'','','Learner Licence')
insert into document values (2,'','','Advanced Certificate')

insert into type values ('Leaners', '50', '1')
insert into type values ('Advanced', '100', '1')

insert into instructor values ('inst1', 'hogan', 'Hulk', 'Hogan','Hulk@gmail.com', 123456789)
insert into instructor values ('inst2', 'taylor', 'Taylor', 'Swift','taylor@gmail.com', 08001234)
insert into instructor values ('inst3', 'grant', 'Carey', 'Grant','Carey@gmail.com', 080089889)
insert into instructor values ('inst4', 'east', 'Clint', 'Eastwood','clint@gmail.com', 080015987)

insert into car values  ('ABC123','Toyota Corolla')
insert into car values  ('ABC456','Toyota Corolla')
insert into car values  ('ABC789','Toyota Corolla')
insert into car values  ('DEF123','Toyota Corolla')
insert into car values  ('DEF456','Toyota Corolla')

insert into client values ('test','t2', 't','2', 't2@gmail.com', '5000', 'Leaners', 1)
insert into client values ('test3','t24', 't','2', 't42@gmail.com', '5000', 'Leaners', 2)

insert into admin values ('admin1', 'poppy123', 'Bob', 'Smith', 'bobsmith@gmail.com' ) 
insert into admin values ('admin2', 'sunflower123', 'Dick', 'Miller', 'bobsmith@gmail.com' )
insert into admin values ('admin3', 'sweetpea123', 'Harry', 'Cooper', 'bobsmith@gmail.com' )

insert into appointment values ('Bilbo',1)

insert into assigned values ('Hobbit', 'ABC123','2017-07-06')

insert into times values ('07:00')
insert into times values ('08:00')
insert into times values ('09:00')
insert into times values ('10:00')
insert into times values ('11:00')
insert into times values ('12:00')
insert into times values ('13:00')
insert into times values ('14:00')
insert into times values ('15:00')
insert into times values ('16:00')
insert into times values ('17:00')
insert into times values ('18:00')
insert into times values ('19:00')

select * from timeslot
select * from document
select * from type
select * from appointment
select * from car
select * from admin
select * from instructor
select * from client
select * from assigned


drop table assigned
drop table client
drop table document
drop table type
drop table appointment
drop table timeslot
drop table car
drop table instructor
drop table admin
drop database D2


select * from times
drop table times



